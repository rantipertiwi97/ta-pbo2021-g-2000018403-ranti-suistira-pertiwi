package com.company;

import java.util.Scanner;

public class PenjahitanBj {
    String Menu="";
    int Pilihan, JumlahBaju, Harga;
    int Total=0;
    Scanner scan = new Scanner(System.in);

    void Menu(){
        System.out.println("        PEMESANAN PENJAHITAN BAJU SIFA TAILOR       ");
        System.out.println("            ------------------------------          ");
        System.out.println("                   Daftar Harga Baju                ");

        System.out.println("No  Model Baju Dan Ukuran                   Harga Baju");
        System.out.println("1.  Croptop XL                            Rp 100.000,-");
        System.out.println("2.  Tunik L                               Rp 150.000,-");
        System.out.println("3.  Kemeja L                              Rp 120.000,-");
        System.out.println("4.  Kaos Cewe M                           Rp  75.000,-");
        System.out.println("5.  Kaos Cowo XL                          Rp  85.000,-");
        System.out.println("6.  Mukena L                              Rp 250.000,-");
        System.out.println("7.  Baju Muslim Anak S                    Rp 240.000,-");
        System.out.println("------------------------------");
        System.out.println("Melakukan Pemesanan Penjahitan Baju ");
        System.out.println("Masukkan Nomor Baju Yang Ingin Anda Pesan :");
        Pilihan = scan.nextInt();
        switch (Pilihan){
            case 1:
                System.out.println("Pesanan Anda Croptop XL     Rp 100.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 100000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                      -TERIMA KASIH-                      ");
                break;
            case 2:
                System.out.println("Pesanan Anda Tunik L        Rp 150.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 150000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera Lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                        -TERIMA KASIH-                    ");
                break;
            case 3:
                System.out.println("Pesanan Anda Kemeja L        Rp 120.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 120000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera Lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                        -TERIMA KASIH-                    ");
                break;
            case 4:
                System.out.println("Pesanan Anda Kaos Cewe M        Rp  75.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 75000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera Lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                        -TERIMA KASIH-                    ");
                break;
            case 5:
                System.out.println("Pesanan Anda Kaos Cowo         Rp   85.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 85000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera Lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                        -TERIMA KASIH-                    ");
                break;
            case 6:
                System.out.println("Pesanan Anda Mukena L        Rp 250.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 250000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera Lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                        -TERIMA KASIH-                    ");
                break;
            case 7:
                System.out.println("Pesanan Anda Baju Muslim Anak S         Rp 240.000,-");
                System.out.println("Masukkan Jumlah Baju Yang Ingin Anda Pesan :");
                JumlahBaju=scan.nextInt();
                Harga= 240000 *JumlahBaju;
                System.out.println("Harga Pesanan = Rp. "+Harga);
                System.out.println("Total Pembayaran Rp "+Harga+" untuk "+JumlahBaju+" Baju");
                Total=Total+Harga;
                System.out.println("Segera Lakukan Pembayaran Melalui Indomaret atau Alfamart ");
                System.out.println("                        -TERIMA KASIH-                    ");
                break;

        }
    }
}